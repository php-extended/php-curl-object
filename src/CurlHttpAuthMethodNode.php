<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlHttpAuthMethodNode class file.
 * 
 * This class is to make the possibility to associate multiple http auth method
 * compliant objects.
 * 
 * @author Anastaszor
 */
class CurlHttpAuthMethodNode implements CurlHttpAuthMethodInterface
{
	
	/**
	 * The left size of the tree node.
	 * 
	 * @var CurlHttpAuthMethodInterface
	 */
	protected CurlHttpAuthMethodInterface $_first;
	
	/**
	 * The right size of the tree node.
	 * 
	 * @var CurlHttpAuthMethodInterface
	 */
	protected CurlHttpAuthMethodInterface $_second;
	
	/**
	 * Builds a new CurlHttpAuthMethodNode with the given other two redirection
	 * objects.
	 * 
	 * @param CurlHttpAuthMethodInterface $first
	 * @param CurlHttpAuthMethodInterface $second
	 */
	public function __construct(CurlHttpAuthMethodInterface $first, CurlHttpAuthMethodInterface $second)
	{
		$this->_first = $first;
		$this->_second = $second;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlHttpAuthMethodInterface::getCurlValue()
	 */
	public function getCurlValue() : int
	{
		return $this->_first->getCurlValue() | $this->_second->getCurlValue();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlHttpAuthMethodInterface::with()
	 */
	public function with(CurlHttpAuthMethodInterface $other) : CurlHttpAuthMethodInterface
	{
		return new self($this, $other);
	}
	
}
