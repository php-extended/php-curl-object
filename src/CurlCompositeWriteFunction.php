<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlCompositeWriteFunction class file.
 * 
 * This class merges two write functions into one.
 * 
 * @author Anastaszor
 */
class CurlCompositeWriteFunction implements CurlWriteFunctionInterface
{
	
	/**
	 * The first write function.
	 * 
	 * @var CurlWriteFunctionInterface
	 */
	protected CurlWriteFunctionInterface $_first;
	
	/**
	 * The second write function.
	 * 
	 * @var CurlWriteFunctionInterface
	 */
	protected CurlWriteFunctionInterface $_second;
	
	/**
	 * Builds a new CurlCompositeWriteFunction with two others write functions.
	 * 
	 * @param CurlWriteFunctionInterface $first
	 * @param CurlWriteFunctionInterface $second
	 */
	public function __construct(CurlWriteFunctionInterface $first, CurlWriteFunctionInterface $second)
	{
		$this->_first = $first;
		$this->_second = $second;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlWriteFunctionInterface::write()
	 */
	public function write(CurlInterface $curl, string $data) : int
	{
		$wt1 = $this->_second->write($curl, $data);
		$wt2 = $this->_second->write($curl, $data);
		
		return \max($wt1, $wt2);
	}
	
}
