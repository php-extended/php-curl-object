<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

use CurlMultiHandle;

/**
 * CurlMulti class file.
 * 
 * This class is a simple implementation of the CurlMultiInterface that uses the
 * underlying curl library.
 * 
 * @author Anastaszor
 */
class CurlMulti implements CurlMultiInterface
{
	
	/**
	 * The curl multi handle.
	 * 
	 * @var CurlMultiHandle
	 */
	protected $_handle;
	
	/**
	 * The stack of other curl objects.
	 * 
	 * @var array<integer, Curl>
	 */
	protected array $_stack = [];
	
	/**
	 * Whether this multi handle is still running.
	 * 
	 * @var integer
	 */
	protected int $_running = 0;
	
	/**
	 * Builds a new CurlMulti object.
	 * 
	 * @throws CurlException if the initialization fails
	 */
	public function __construct()
	{
		$this->_handle = \curl_multi_init();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlMultiInterface::addHandle()
	 */
	public function addHandle(CurlInterface $handle) : bool
	{
		if(!$handle instanceof Curl)
		{
			throw new CurlException('Unexpected handle type of class '.\get_class($handle).', should be an instance of '.Curl::class);
		}
		
		$res = \curl_multi_add_handle($this->_handle, $handle->getHandle());
		if(\CURLM_OK === $res)
		{
			$this->_stack[] = $handle;
			
			return true;
		}
		
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlMultiInterface::removeHandle()
	 */
	public function removeHandle(CurlInterface $handle) : bool
	{
		if(!$handle instanceof Curl)
		{
			throw new CurlException('Unexpected handle type of class '.\get_class($handle).', should be an instance of '.Curl::class);
		}
		
		$res = \curl_multi_remove_handle($this->_handle, $handle->getHandle());
		if(\CURLM_OK === $res)
		{
			$handleHash = \spl_object_hash($handle);
			
			foreach($this->_stack as $k => $object)
			{
				if(\spl_object_hash($object) === $handleHash)
				{
					unset($this->_stack[$k]);
				}
			}
			
			return true;
		}
		
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlMultiInterface::execute()
	 */
	public function execute() : bool
	{
		$res = \curl_multi_exec($this->_handle, $this->_running);
		
		return \CURLM_OK === $res;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlMultiInterface::getErrorCode()
	 */
	public function getErrorCode() : int
	{
		return (int) \curl_multi_errno($this->_handle);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlMultiInterface::getErrorMessage()
	 */
	public function getErrorMessage() : string
	{
		return (string) \curl_multi_strerror($this->getErrorCode());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlMultiInterface::isStillRunning()
	 */
	public function isStillRunning() : bool
	{
		return 0 < $this->_running;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlMultiInterface::getInfo()
	 */
	public function getInfo() : CurlMultiInfoInterface
	{
		$nbmsg = 0;
		$info = \curl_multi_info_read($this->_handle, $nbmsg);
		if(false === $info || !isset($info['msg']) || !isset($info['result']) || !isset($info['handle']))
		{
			throw new CurlException('Failed to get multi info from curl multi handle.');
		}
		
		/** @psalm-suppress MixedArgument */
		return new CurlMultiInfo((string) $info['msg'], (int) $info['result'], new Curl($info['handle']));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlMultiInterface::disablePipelining()
	 */
	public function disablePipelining() : bool
	{
		return \curl_multi_setopt($this->_handle, \CURLMOPT_PIPELINING, \CURLPIPE_NOTHING);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlMultiInterface::enablePipelining()
	 */
	public function enablePipelining() : bool
	{
		return \curl_multi_setopt($this->_handle, \CURLMOPT_PIPELINING, \CURLPIPE_HTTP1);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlMultiInterface::enableMultiplexing()
	 */
	public function enableMultiplexing() : bool
	{
		return \curl_multi_setopt($this->_handle, \CURLMOPT_PIPELINING, \CURLPIPE_MULTIPLEX);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlMultiInterface::setMaxSimultaneousConnections()
	 */
	public function setMaxSimultaneousConnections(int $maxConnections) : bool
	{
		return \curl_multi_setopt($this->_handle, \CURLMOPT_MAXCONNECTS, $maxConnections);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlMultiInterface::setChunkLengthPenaltySize()
	 */
	public function setChunkLengthPenaltySize(int $sizeInBytes) : bool
	{
		return \curl_multi_setopt($this->_handle, \CURLMOPT_CHUNK_LENGTH_PENALTY_SIZE, $sizeInBytes);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlMultiInterface::setContentLengthPenaltySize()
	 */
	public function setContentLengthPenaltySize(int $sizeInBytes) : bool
	{
		return \curl_multi_setopt($this->_handle, \CURLMOPT_CONTENT_LENGTH_PENALTY_SIZE, $sizeInBytes);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlMultiInterface::setMaxHostConnections()
	 */
	public function setMaxHostConnections(int $maxConnections) : bool
	{
		return \curl_multi_setopt($this->_handle, \CURLMOPT_MAX_HOST_CONNECTIONS, $maxConnections);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlMultiInterface::setMaxPipelineLength()
	 */
	public function setMaxPipelineLength(int $maxNbRequests) : bool
	{
		return \curl_multi_setopt($this->_handle, \CURLMOPT_MAX_PIPELINE_LENGTH, $maxNbRequests);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlMultiInterface::setMaxTotalConnections()
	 */
	public function setMaxTotalConnections(int $maxConnections) : bool
	{
		return \curl_multi_setopt($this->_handle, \CURLMOPT_MAX_TOTAL_CONNECTIONS, $maxConnections);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlMultiInterface::setPushFunction()
	 * @throws CurlException
	 */
	public function setPushFunction(CurlMultiPushFunctionInterface $callable) : bool
	{
		return \curl_multi_setopt(
			$this->_handle,
			\CURLMOPT_PUSHFUNCTION,
			/** @psalm-suppress MissingClosureParamType */
			function($parentCh, $pushedCh, array $headers) use ($callable)
			{
				$parentObject = null;
				
				foreach($this->_stack as $handle)
				{
					if($handle->getHandle() === $parentCh)
					{
						$parentObject = $handle;
					}
				}
				
				if(null === $parentObject)
				{
					return false;
				}
				
				/** @psalm-suppress MixedArgument,MixedArgumentTypeCoercion */
				return $callable->accept($parentObject, new Curl($pushedCh), $headers);
			},
		);
	}
	
	/**
	 * Closes the handle on destruction.
	 */
	public function __destruct()
	{
		\curl_multi_close($this->_handle);
	}
	
}
