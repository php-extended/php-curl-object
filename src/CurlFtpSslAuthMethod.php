<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlFtpSslAuthMethod class file.
 * 
 * This class represents the curl ftp ssl auth methods that are allowed in curl.
 * 
 * @author Anastaszor
 */
enum CurlFtpSslAuthMethod : int implements CurlFtpSslAuthMethodInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlFtpSslAuthMethodInterface::getCurlValue()
	 */
	public function getCurlValue() : int
	{
		return $this->value;
	}

	case SSL_FIRST = \CURLFTPAUTH_SSL;
	case TLS_FIRST = \CURLFTPAUTH_TLS;
	case DEFAULT = \CURLFTPAUTH_DEFAULT;
	
}
