<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlHttpAuthMethod class file.
 * 
 * This class represents the curl http auth methods that are allowed in curl.
 * 
 * @author Anastaszor
 */
enum CurlHttpAuthMethod : int implements CurlHttpAuthMethodInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlHttpAuthMethodInterface::getCurlValue()
	 */
	public function getCurlValue() : int
	{
		return $this->value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlHttpAuthMethodInterface::with()
	 */
	public function with(CurlHttpAuthMethodInterface $other) : CurlHttpAuthMethodInterface
	{
		return new CurlHttpAuthMethodNode($this, $other);
	}

	case ANY = \CURLAUTH_ANY;
	case ANYSAFE = \CURLAUTH_ANYSAFE;
	case BASIC = \CURLAUTH_BASIC;
	case DIGEST = \CURLAUTH_DIGEST;
	case DIGEST_IE = \CURLAUTH_DIGEST_IE;
	case NEGOTIATE = \CURLAUTH_NEGOTIATE;
	case NONE = \CURLAUTH_NONE;
	case NTLM = \CURLAUTH_NTLM;
	case NTLM_WB = \CURLAUTH_NTLM_WB;
	case ONLY = \CURLAUTH_ONLY;
	
}
