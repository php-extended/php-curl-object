<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlMultiInfo class file.
 * 
 * This class is a simple implementation of the CurlMultiInfoInterface that
 * uses the underlying curl library.
 * 
 * @author Anastaszor
 */
class CurlMultiInfo implements CurlMultiInfoInterface
{
	
	/**
	 * The message.
	 * 
	 * @var string
	 */
	protected string $_message;
	
	/**
	 * The error code.
	 * 
	 * @var integer
	 */
	protected int $_result;
	
	/**
	 * The handle.
	 * 
	 * @var CurlInterface
	 */
	protected CurlInterface $_handle;
	
	/**
	 * Builds a new CurlMultiInfoHandle with the given data.
	 * 
	 * @param string $message
	 * @param int $result
	 * @param CurlInterface $handle
	 */
	public function __construct(string $message, int $result, CurlInterface $handle)
	{
		$this->_message = $message;
		$this->_result = $result;
		$this->_handle = $handle;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlMultiInfoInterface::getMessage()
	 */
	public function getMessage() : string
	{
		return $this->_message;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlMultiInfoInterface::getResult()
	 */
	public function getResult() : int
	{
		return $this->_result;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlMultiInfoInterface::getHandle()
	 */
	public function getHandle() : CurlInterface
	{
		return $this->_handle;
	}
	
}
