<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlProtocolNode class file.
 * 
 * This class is to make the possibility to associate multiple protocol
 * compliant objects.
 * 
 * @author Anastaszor
 */
class CurlProtocolNode implements CurlProtocolInterface
{
	
	/**
	 * The left size of the tree node.
	 * 
	 * @var CurlProtocolInterface
	 */
	protected CurlProtocolInterface $_first;
	
	/**
	 * The right size of the tree node.
	 * 
	 * @var CurlProtocolInterface
	 */
	protected CurlProtocolInterface $_second;
	
	/**
	 * Builds a new CurlProtocolNode with the given other two redirection
	 * objects.
	 * 
	 * @param CurlProtocolInterface $first
	 * @param CurlProtocolInterface $second
	 */
	public function __construct(CurlProtocolInterface $first, CurlProtocolInterface $second)
	{
		$this->_first = $first;
		$this->_second = $second;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlProtocolInterface::getCurlValue()
	 */
	public function getCurlValue() : int
	{
		return $this->_first->getCurlValue() | $this->_second->getCurlValue();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlProtocolInterface::with()
	 */
	public function with(CurlProtocolInterface $other) : CurlProtocolInterface
	{
		return new self($this, $other);
	}
	
}
