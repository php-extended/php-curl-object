<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlHttpVersion class file.
 * 
 * This class represents the curl http version that are allowed in curl.
 * 
 * @author Anastaszor
 */
enum CurlHttpVersion : int implements CurlHttpVersionInterface
{
	
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlHttpVersionInterface::getCurlValue()
	 */
	public function getCurlValue() : int
	{
		return $this->value;
	}

	case HTTP_1_0 = \CURL_HTTP_VERSION_1_0;
	case HTTP_1_1 = \CURL_HTTP_VERSION_1_1;
	case HTTP_2_0 = \CURL_HTTP_VERSION_2_0;
	case HTTP_2_TLS = \CURL_HTTP_VERSION_2TLS;
	case HTTP_2_PRIOR = \CURL_HTTP_VERSION_2_PRIOR_KNOWLEDGE;
	
}
