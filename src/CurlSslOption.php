<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlSslOption class file.
 * 
 * This class represents ssl options that are allowed in curl.
 * 
 * @author Anastaszor
 */
enum CurlSslOption : int implements CurlSslOptionInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlSslOptionInterface::getCurlValue()
	 */
	public function getCurlValue() : int
	{
		return $this->value;
	}
	case ALLOW_BEAST = \CURLSSLOPT_ALLOW_BEAST;
	case NO_REVOKE = \CURLSSLOPT_NO_REVOKE;
	
}
