<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

use CurlHandle;
use DateTimeInterface;
use PhpExtended\HttpMessage\FileStream;
use PhpExtended\HttpMessage\StringStream;
use PhpExtended\Ip\Ipv4AddressInterface;
use PhpExtended\Ip\Ipv6AddressInterface;
use PhpExtended\UserAgent\UserAgentInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UriInterface;
use RuntimeException;

/**
 * Curl class file.
 * 
 * This class is a simple implementation of the CurlInterface that uses the
 * underlying curl library.
 * 
 * @author Anastaszor
 * @psalm-suppress PropertyNotSetInConstructor
 * @SuppressWarnings("PHPMD.ExcessiveClassLength")
 * @SuppressWarnings("PHPMD.ExcessivePublicCount")
 * @SuppressWarnings("PHPMD.TooManyMethods")
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 * @SuppressWarnings("PHPMD.TooManyPublicMethods")
 */
class Curl implements CurlInterface
{
	
	/**
	 * The curl handle.
	 * 
	 * @var CurlHandle
	 */
	protected $_handle;
	
	/**
	 * True on success or false on failure However, if the CURLOPT_RETURNTRANSFER
	 * option is set, it will return the result on success, false on failure.
	 * 
	 * @var boolean|string
	 */
	protected $_result = '';
	
	/**
	 * The handle that is set with CURLOPT_FILE, when needed.
	 * 
	 * @var ?resource
	 */
	protected $_file;
	
	/**
	 * Builds a new Curl object.
	 * 
	 * @param ?CurlHandle $curlHandle
	 * @throws CurlException if the init cannot be done
	 */
	public function __construct($curlHandle = null)
	{	
		if(null === $curlHandle)
		{
			$curlHandle = \curl_init();
			/** @phpstan-ignore-next-line */
			if(false === $curlHandle)
			{
				throw new CurlException('Failed to initialize CURL');
			}
		}
		
		$this->_handle = $curlHandle;
		
		// always set return transfer to true, we want the return in the
		// getData method
		\curl_setopt($this->_handle, \CURLOPT_RETURNTRANSFER, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the inner handle of this object, this is used in the multi 
	 * handle handling.
	 * 
	 * @return CurlHandle
	 */
	public function getHandle() : CurlHandle
	{
		return $this->_handle;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::execute()
	 */
	public function execute() : bool
	{
		$this->_result = \curl_exec($this->_handle);
		
		return !(false === $this->_result);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::getErrorCode()
	 */
	public function getErrorCode() : int
	{
		return \curl_errno($this->_handle);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::getErrorMessage()
	 */
	public function getErrorMessage() : string
	{
		return \curl_error($this->_handle);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::getData()
	 * @throws RuntimeException
	 */
	public function getData() : StreamInterface
	{
		if(null !== $this->_file)
		{
			return new FileStream($this->_file);
		}
		
		return new StringStream((string) $this->_result);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableAutoreferer()
	 */
	public function enableAutoreferer() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_AUTOREFERER, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableAutoreferer()
	 */
	public function disableAutoreferer() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_AUTOREFERER, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableCookieSession()
	 */
	public function enableCookieSession() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_COOKIESESSION, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableCookieSession()
	 */
	public function disableCookieSession() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_COOKIESESSION, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableCertInfo()
	 */
	public function enableCertInfo() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_CERTINFO, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableCertInfo()
	 */
	public function disableCertInfo() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_CERTINFO, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableConnectOnly()
	 */
	public function enableConnectOnly() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_CONNECT_ONLY, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableConnectOnly()
	 */
	public function disableConnectOnly() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_CONNECT_ONLY, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableCrlf()
	 */
	public function enableCrlf() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_CRLF, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableCrlf()
	 */
	public function disableCrlf() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_CRLF, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableDnsUseGlobalCache()
	 */
	public function enableDnsUseGlobalCache() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_DNS_USE_GLOBAL_CACHE, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableDnsUseGlobalCache()
	 */
	public function disableDnsUseGlobalCache() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_DNS_USE_GLOBAL_CACHE, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableFailOnError()
	 */
	public function enableFailOnError() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_FAILONERROR, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableFailOnError()
	 */
	public function disableFailOnError() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_FAILONERROR, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableSslFalseStart()
	 */
	public function enableSslFalseStart() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_SSL_FALSESTART, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableSslFalseStart()
	 */
	public function disableSslFalseStart() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_SSL_FALSESTART, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableFiletime()
	 */
	public function enableFiletime() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_FILETIME, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableFiletime()
	 */
	public function disableFiletime() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_FILETIME, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableFollowLocation()
	 */
	public function enableFollowLocation() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_FOLLOWLOCATION, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableFollowLocation()
	 */
	public function disableFollowLocation() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_FOLLOWLOCATION, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableForbidReuse()
	 */
	public function enableForbidReuse() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_FORBID_REUSE, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableForbidReuse()
	 */
	public function disableForbidReuse() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_FORBID_REUSE, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableFreshConnect()
	 */
	public function enableFreshConnect() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_FRESH_CONNECT, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableFreshConnect()
	 */
	public function disableFreshConnect() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_FRESH_CONNECT, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableFtpUseEprt()
	 */
	public function enableFtpUseEprt() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_FTP_USE_EPRT, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableFtpUseEprt()
	 */
	public function disableFtpUseEprt() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_FTP_USE_EPRT, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableFtpUseEpsv()
	 */
	public function enableFtpUseEpsv() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_FTP_USE_EPSV, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableFtpUseEpsv()
	 */
	public function disableFtpUseEpsv() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_FTP_USE_EPSV, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableFtpCreateMissingDirs()
	 */
	public function enableFtpCreateMissingDirs() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_FTP_CREATE_MISSING_DIRS, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableFtpCreateMissingDirs()
	 */
	public function disableFtpCreateMissingDirs() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_FTP_CREATE_MISSING_DIRS, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableFtpAppend()
	 */
	public function enableFtpAppend() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_FTPAPPEND, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableFtpAppend()
	 */
	public function disableFtpAppend() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_FTPAPPEND, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableTcpNodelay()
	 */
	public function enableTcpNodelay() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_TCP_NODELAY, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableTcpNodelay()
	 */
	public function disableTcpNodelay() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_TCP_NODELAY, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableFtpListOnly()
	 */
	public function enableFtpListOnly() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_FTPLISTONLY, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableFtpListOnly()
	 */
	public function disableFtpListOnly() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_FTPLISTONLY, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableHeader()
	 */
	public function enableHeader() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_HEADER, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableHeader()
	 */
	public function disableHeader() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_HEADER, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableHeaderOut()
	 */
	public function enableHeaderOut() : bool
	{
		return \curl_setopt($this->_handle, \CURLINFO_HEADER_OUT, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableHeaderOut()
	 */
	public function disableHeaderOut() : bool
	{
		return \curl_setopt($this->_handle, \CURLINFO_HEADER_OUT, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableHttpProxyTunnel()
	 */
	public function enableHttpProxyTunnel() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_HTTPPROXYTUNNEL, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableHttpProxyTunnel()
	 */
	public function disableHttpProxyTunnel() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_HTTPPROXYTUNNEL, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableNetrc()
	 */
	public function enableNetrc() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_NETRC, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableNetrc()
	 */
	public function disableNetrc() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_NETRC, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableNoBody()
	 */
	public function enableNoBody() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_NOBODY, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableNoBody()
	 */
	public function disableNoBody() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_NOBODY, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableNoProgress()
	 */
	public function enableNoProgress() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_NOPROGRESS, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableNoProgress()
	 */
	public function disableNoProgress() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_NOPROGRESS, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableNoSignal()
	 */
	public function enableNoSignal() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_NOSIGNAL, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableNoSignal()
	 */
	public function disableNoSignal() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_NOSIGNAL, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enablePathAsIs()
	 */
	public function enablePathAsIs() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_PATH_AS_IS, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disablePathAsIs()
	 */
	public function disablePathAsIs() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_PATH_AS_IS, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enablePipeWait()
	 */
	public function enablePipeWait() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_PIPEWAIT, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disablePipeWait()
	 */
	public function disablePipeWait() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_PIPEWAIT, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableGet()
	 */
	public function enableGet() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_HTTPGET, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enablePost()
	 */
	public function enablePost() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_POST, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enablePut()
	 */
	public function enablePut() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_PUT, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableSaslIr()
	 */
	public function enableSaslIr() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_SASL_IR, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableSaslIr()
	 */
	public function disableSaslIr() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_SASL_IR, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableSslAlpn()
	 */
	public function enableSslAlpn() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_SSL_ENABLE_ALPN, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableSslAlpn()
	 */
	public function disableSslAlpn() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_SSL_ENABLE_ALPN, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableSslNpn()
	 */
	public function enableSslNpn() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_SSL_ENABLE_NPN, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableSslNpn()
	 */
	public function disableSslNpn() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_SSL_ENABLE_NPN, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableSslVerifyPeer()
	 */
	public function enableSslVerifyPeer() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_SSL_VERIFYPEER, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableSslVerifyPeer()
	 */
	public function disableSslVerifyPeer() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_SSL_VERIFYPEER, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableSslVerifyStatus()
	 */
	public function enableSslVerifyStatus() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_SSL_VERIFYSTATUS, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableSslVerifyStatus()
	 */
	public function disableSslVerifyStatus() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_SSL_VERIFYSTATUS, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableTcpFastOpen()
	 */
	public function enableTcpFastOpen() : bool
	{
		// CURLOPT_TCP_FASTOPEN since 7.0.7
		// https://github.com/curl/curl/blob/03564deba2039e3530bb166ddaddd0ec58ff045c/include/curl/curl.h#L1802
		return \curl_setopt($this->_handle, \CURLOPT_TCP_FASTOPEN, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableTcpFastOpen()
	 */
	public function disableTcpFastOpen() : bool
	{
		// CURLOPT_TCP_FASTOPEN since 7.0.7
		// https://github.com/curl/curl/blob/03564deba2039e3530bb166ddaddd0ec58ff045c/include/curl/curl.h#L1802
		return \curl_setopt($this->_handle, \CURLOPT_TCP_FASTOPEN, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableTftpNoOptions()
	 */
	public function enableTftpNoOptions() : bool
	{
		// https://github.com/curl/curl/blob/03564deba2039e3530bb166ddaddd0ec58ff045c/include/curl/curl.h#L1795
		return \curl_setopt($this->_handle, \CURLOPT_TFTP_NO_OPTIONS, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableTftpNoOptions()
	 */
	public function disableTftpNoOptions() : bool
	{
		// https://github.com/curl/curl/blob/03564deba2039e3530bb166ddaddd0ec58ff045c/include/curl/curl.h#L1795
		return \curl_setopt($this->_handle, \CURLOPT_TFTP_NO_OPTIONS, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableTransferText()
	 */
	public function enableTransferText() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_TRANSFERTEXT, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableTransferText()
	 */
	public function disableTransferText() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_TRANSFERTEXT, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableUnrestrictedAuth()
	 */
	public function enableUnrestrictedAuth() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_UNRESTRICTED_AUTH, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableUnrestrictedAuth()
	 */
	public function disableUnrestrictedAuth() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_UNRESTRICTED_AUTH, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::enableVerbose()
	 */
	public function enableVerbose() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_VERBOSE, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::disableVerbose()
	 */
	public function disableVerbose() : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_VERBOSE, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setBufferSize()
	 */
	public function setBufferSize(int $bufferSizeBytes) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_BUFFERSIZE, $bufferSizeBytes);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setConnectionTimeout()
	 */
	public function setConnectionTimeout(int $timeoutSeconds) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_CONNECTTIMEOUT, $timeoutSeconds);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setConnectionTimeoutMs()
	 */
	public function setConnectionTimeoutMs(int $timeoutMilliSeconds) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_CONNECTTIMEOUT_MS, $timeoutMilliSeconds);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setDnsCacheTimeout()
	 */
	public function setDnsCacheTimeout(int $timeoutSeconds) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_DNS_CACHE_TIMEOUT, $timeoutSeconds);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setExpect100TimeoutMs()
	 */
	public function setExpect100TimeoutMs(int $timeoutMilliSeconds) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_EXPECT_100_TIMEOUT_MS, $timeoutMilliSeconds);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setFtpSslAuth()
	 */
	public function setFtpSslAuth(CurlFtpSslAuthMethodInterface $ftpSslAuthMethod) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_FTPSSLAUTH, $ftpSslAuthMethod->getCurlValue());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setHeaderOpt()
	 */
	public function setHeaderOpt(CurlHeaderOptionInterface $optionMask) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_HEADEROPT, $optionMask->getCurlValue());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setHttpVersion()
	 */
	public function setHttpVersion(CurlHttpVersionInterface $httpVersion) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_HTTP_VERSION, $httpVersion->getCurlValue());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setHttpAuth()
	 */
	public function setHttpAuth(CurlHttpAuthMethodInterface $authMethod) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_HTTPAUTH, $authMethod->getCurlValue());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setInFileSize()
	 */
	public function setInFileSize(int $fileSizeBytes) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_INFILESIZE, $fileSizeBytes);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setLowSpeedLimit()
	 */
	public function setLowSpeedLimit(int $byteSize) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_LOW_SPEED_LIMIT, $byteSize);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setLowSpeedTime()
	 */
	public function setLowSpeedTime(int $seconds) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_LOW_SPEED_TIME, $seconds);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setMaxConnects()
	 */
	public function setMaxConnects(int $nbConnects) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_MAXCONNECTS, $nbConnects);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setMaxRedirs()
	 */
	public function setMaxRedirs(int $nbRedirs) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_MAXREDIRS, $nbRedirs);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setPort()
	 */
	public function setPort(int $port) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_PORT, $port);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setPostRedir()
	 */
	public function setPostRedir(CurlPostRedirectionInterface $postRedir) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_POSTREDIR, $postRedir->getCurlValue());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setProtocols()
	 */
	public function setProtocols(CurlProtocolInterface $protocol) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_PROTOCOLS, $protocol->getCurlValue());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setProxyAuth()
	 */
	public function setProxyAuth(CurlHttpAuthMethodInterface $proxyAuthMethod) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_PROXYAUTH, $proxyAuthMethod->getCurlValue());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setProxyPort()
	 */
	public function setProxyPort(int $proxyPort) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_PROXYPORT, $proxyPort);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setProxyType()
	 */
	public function setProxyType(CurlProxyTypeInterface $proxyType) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_PROXYTYPE, $proxyType->getCurlValue());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setRedirProtocols()
	 */
	public function setRedirProtocols(CurlProtocolInterface $protocol) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_REDIR_PROTOCOLS, $protocol->getCurlValue());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setResumeFrom()
	 */
	public function setResumeFrom(int $offsetBytes) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_RESUME_FROM, $offsetBytes);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setSslOptions()
	 */
	public function setSslOptions(CurlSslOptionInterface $option) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_SSL_OPTIONS, $option->getCurlValue());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setSslVerifyHost()
	 */
	public function setSslVerifyHost(CurlSslVerifyHostMethodInterface $verifyMask) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_SSL_VERIFYHOST, $verifyMask->getCurlValue());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setSslVersion()
	 */
	public function setSslVersion(CurlSslVersionInterface $sslVersion) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_SSLVERSION, $sslVersion->getCurlValue());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setStreamWeight()
	 */
	public function setStreamWeight(int $weight) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_STREAM_WEIGHT, $weight);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setTimeCondition()
	 */
	public function setTimeCondition(CurlTimeConditionInterface $timeCondition) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_TIMECONDITION, $timeCondition->getCurlValue());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setTimeout()
	 */
	public function setTimeout(int $timeoutSeconds) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_TIMEOUT, $timeoutSeconds);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setTimeoutMs()
	 */
	public function setTimeoutMs(int $timeoutMilliSeconds) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_TIMEOUT_MS, $timeoutMilliSeconds);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setTimeValue()
	 */
	public function setTimeValue(DateTimeInterface $timestamp) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_TIMEVALUE, $timestamp->getTimestamp());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setMaxRecvSpeedLarge()
	 */
	public function setMaxRecvSpeedLarge(int $maxSpeedBytesPerSec) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_MAX_RECV_SPEED_LARGE, $maxSpeedBytesPerSec);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setMaxSendSpeedLarge()
	 */
	public function setMaxSendSpeedLarge(int $maxSpeedBytesPerSec) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_MAX_SEND_SPEED_LARGE, $maxSpeedBytesPerSec);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setSshAuthTypes()
	 */
	public function setSshAuthTypes(CurlSslAuthTypeInterface $authType) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_SSH_AUTH_TYPES, $authType->getCurlValue());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setIpResolve()
	 */
	public function setIpResolve(CurlIpResolveMethodInterface $ipResolvMask) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_IPRESOLVE, $ipResolvMask->getCurlValue());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setFtpFileMethod()
	 */
	public function setFtpFileMethod(CurlFtpFileMethodInterface $fileMethod) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_FTP_FILEMETHOD, $fileMethod->getCurlValue());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setCaInfo()
	 */
	public function setCaInfo(string $caFilePath) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_CAINFO, $caFilePath);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setCaPath()
	 */
	public function setCaPath(string $caDirPath) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_CAPATH, $caDirPath);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setCookie()
	 */
	public function setCookie(string $cookieString) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_COOKIE, $cookieString);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setCookieFile()
	 */
	public function setCookieFile(string $cookieFilePath) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_COOKIEFILE, $cookieFilePath);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setCookieJarFile()
	 */
	public function setCookieJarFile(string $cookieJarFilePath) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_COOKIEJAR, $cookieJarFilePath);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setCustomRequest()
	 */
	public function setCustomRequest(string $request) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_CUSTOMREQUEST, $request);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setDefaultProtocol()
	 */
	public function setDefaultProtocol(string $defaultProtocol) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_DEFAULT_PROTOCOL, $defaultProtocol);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setDnsInterface()
	 */
	public function setDnsInterface(string $dnsInterface) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_DNS_INTERFACE, $dnsInterface);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setDnsLocalIp4()
	 */
	public function setDnsLocalIp4(Ipv4AddressInterface $ipv4) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_DNS_LOCAL_IP4, $ipv4->getCanonicalRepresentation());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setDnsLocalIp6()
	 */
	public function setDnsLocalIp6(Ipv6AddressInterface $ipv6) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_DNS_LOCAL_IP6, $ipv6->getCanonicalRepresentation());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setEgdSocket()
	 */
	public function setEgdSocket(string $socketName) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_EGDSOCKET, $socketName);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setEncoding()
	 */
	public function setEncoding(string $encodings) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_ENCODING, $encodings);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setFtpPort()
	 */
	public function setFtpPort(string $port) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_FTPPORT, $port);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setInterface()
	 */
	public function setInterface(string $interface) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_INTERFACE, $interface);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setKeyPassword()
	 */
	public function setKeyPassword(string $password) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_KEYPASSWD, $password);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setKrb4Level()
	 */
	public function setKrb4Level(string $kerberosLevel) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_KRB4LEVEL, $kerberosLevel);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setLoginOptions()
	 */
	public function setLoginOptions(string $loginOptions) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_LOGIN_OPTIONS, $loginOptions);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setPinnedPublicKey()
	 */
	public function setPinnedPublicKey(string $pinnedPublicKey) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_PINNEDPUBLICKEY, $pinnedPublicKey);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setPostFields()
	 */
	public function setPostFields(string $postFields) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_POSTFIELDS, $postFields);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setPrivate()
	 */
	public function setPrivate(string $privateData) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_PRIVATE, $privateData);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setProxy()
	 */
	public function setProxy(string $proxyUrl) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_PROXY, $proxyUrl);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setProxyServiceName()
	 */
	public function setProxyServiceName(string $proxyServiceName) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_PROXY_SERVICE_NAME, $proxyServiceName);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setProxyUserPassword()
	 */
	public function setProxyUserPassword(string $proxyUserPassword) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_PROXYUSERPWD, $proxyUserPassword);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setRandomFile()
	 */
	public function setRandomFile(string $filePath) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_RANDOM_FILE, $filePath);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setRange()
	 */
	public function setRange(string $rangeIntervalList) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_RANGE, $rangeIntervalList);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setReferer()
	 */
	public function setReferer(string $referer) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_REFERER, $referer);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setServiceName()
	 */
	public function setServiceName(string $authServiceName) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_SERVICE_NAME, $authServiceName);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setSshHostPublicKeyMd5()
	 */
	public function setSshHostPublicKeyMd5(string $md5Hash) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_SSH_HOST_PUBLIC_KEY_MD5, $md5Hash);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setSshPublicKeyfile()
	 */
	public function setSshPublicKeyfile(string $publicKeyFilePath) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_SSH_PUBLIC_KEYFILE, $publicKeyFilePath);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setSshPrivateKeyfile()
	 */
	public function setSshPrivateKeyfile(string $privateKeyFilePath) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_SSH_PRIVATE_KEYFILE, $privateKeyFilePath);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setSslCipherList()
	 */
	public function setSslCipherList(string $cipherList) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_SSL_CIPHER_LIST, $cipherList);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setSslCert()
	 */
	public function setSslCert(string $pemFileName) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_SSLCERT, $pemFileName);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setSslCertPasswd()
	 */
	public function setSslCertPasswd(string $pemFilePasswd) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_SSLCERTPASSWD, $pemFilePasswd);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setSslCertType()
	 */
	public function setSslCertType(CurlSslKeyTypeInterface $certType) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_SSLCERTTYPE, $certType->getCurlValue());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setSslEngine()
	 */
	public function setSslEngine(string $engine) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_SSLENGINE, $engine);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setSslEngineDefault()
	 */
	public function setSslEngineDefault(string $engine) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_SSLENGINE_DEFAULT, $engine);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setSslKey()
	 */
	public function setSslKey(string $fileName) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_SSLKEY, $fileName);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setSslKeyPassword()
	 */
	public function setSslKeyPassword(string $passwd) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_SSLKEYPASSWD, $passwd);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setSslKeyType()
	 */
	public function setSslKeyType(CurlSslKeyTypeInterface $type) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_SSLKEYTYPE, $type->getCurlValue());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setUnixSocketPath()
	 */
	public function setUnixSocketPath(string $fullPath) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_UNIX_SOCKET_PATH, $fullPath);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setUrl()
	 */
	public function setUrl(UriInterface $uri) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_URL, $uri->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setUserAgent()
	 */
	public function setUserAgent(UserAgentInterface $userAgent) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_USERAGENT, $userAgent->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setUsername()
	 */
	public function setUsername(string $username) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_USERNAME, $username);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setUserPassword()
	 */
	public function setUserPassword(string $userPassword) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_USERPWD, $userPassword);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setXOAuthH2Bearer()
	 */
	public function setXOAuthH2Bearer(string $token) : bool
	{
		/** @phpstan-ignore-next-line */
		return \curl_setopt($this->_handle, \CURLOPT_XOAUTH2_BEARER, $token);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setConnectTo()
	 */
	public function setConnectTo(array $bridges) : bool
	{
		// https://github.com/curl/curl/blob/03564deba2039e3530bb166ddaddd0ec58ff045c/include/curl/curl.h#L1799
		return \curl_setopt($this->_handle, \CURLOPT_CONNECT_TO, $bridges);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setHttp200Aliases()
	 */
	public function setHttp200Aliases(array $aliases) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_HTTP200ALIASES, $aliases);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setHttpHeader()
	 */
	public function setHttpHeader(array $headers) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_HTTPHEADER, $headers);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setProxyHeaders()
	 */
	public function setProxyHeaders(array $headers) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_PROXYHEADER, $headers);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setPostQuote()
	 */
	public function setPostQuote(array $commands) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_POSTQUOTE, $commands);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setQuote()
	 */
	public function setQuote(array $commands) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_QUOTE, $commands);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setResolve()
	 */
	public function setResolve(array $addresses) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_RESOLVE, $addresses);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setFile()
	 */
	public function setFile($resource) : bool
	{
		if(\curl_setopt($this->_handle, \CURLOPT_FILE, $resource))
		{
			$this->_file = $resource;
			
			return true;
		}
		
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setInFile()
	 */
	public function setInFile($resource) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_INFILE, $resource);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setStdErr()
	 */
	public function setStdErr($resource) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_STDERR, $resource);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setWriteHeader()
	 */
	public function setWriteHeader($resource) : bool
	{
		return \curl_setopt($this->_handle, \CURLOPT_WRITEHEADER, $resource);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setHeaderFunction()
	 * @SuppressWarnings("PHPMD.UnusedLocalVariable")
	 */
	public function setHeaderFunction(CurlHeaderFunctionInterface $callback) : bool
	{
		return \curl_setopt(
			$this->_handle,
			\CURLOPT_HEADERFUNCTION,
			/** @psalm-suppress MissingClosureParamType */
			function($curl, string $headerStr) use ($callback) : int
			{
				return $callback->write($this, $headerStr);
			},
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setPasswordFunction()
	 * @SuppressWarnings("PHPMD.UnusedLocalVariable")
	 */
	public function setPasswordFunction(CurlPasswordFunctionInterface $callback) : bool
	{
		return \defined('CURLOPT_PASSWDFUNCTION') || \curl_setopt(
			$this->_handle,
			(int) CURLOPT_PASSWDFUNCTION,
			/** @psalm-suppress MissingClosureParamType */
			function($curl, string $prompt, int $maxlength) use ($callback) : string
			{
				return $callback->prompt($this, $prompt, $maxlength);
			},
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setProgressFunction()
	 * @SuppressWarnings("PHPMD.UnusedLocalVariable")
	 */
	public function setProgressFunction(CurlProgressFunctionInterface $callback) : bool
	{
		return \curl_setopt(
			$this->_handle,
			\CURLOPT_PROGRESSFUNCTION,
			/** @psalm-suppress MissingClosureParamType */
			function($curl, int $totalDownloadBytes, int $actualDownloadBytes, int $totalUploadBytes, int $actualUploadBytes) use ($callback) : int
			{
				return $callback->progress($this, $totalDownloadBytes, $actualDownloadBytes, $totalUploadBytes, $actualUploadBytes);
			},
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setReadFunction()
	 * @SuppressWarnings("PHPMD.UnusedLocalVariable")
	 */
	public function setReadFunction(CurlReadFunctionInterface $callback) : bool
	{
		return \curl_setopt(
			$this->_handle,
			\CURLOPT_READFUNCTION,
			/** @psalm-suppress MissingClosureParamType,MixedArgument */
			function($curl, $file, int $maxlength) use ($callback) : string
			{
				return $callback->read($this, $file, $maxlength);
			},
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setWriteFunction()
	 * @SuppressWarnings("PHPMD.UnusedLocalVariable")
	 */
	public function setWriteFunction(CurlWriteFunctionInterface $callback) : bool
	{
		return \curl_setopt(
			$this->_handle,
			\CURLOPT_WRITEFUNCTION,
			/** @psalm-suppress MissingClosureParamType */
			function($curl, string $data) use ($callback) : int
			{
				return $callback->write($this, $data);
			},
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlInterface::setSharedHandle()
	 * @throws CurlException
	 */
	public function setSharedHandle(CurlSharedInterface $handle) : bool
	{
		if(!$handle instanceof CurlShared)
		{
			throw new CurlException('Unexpected handle type of class '.\get_class($handle).', should be an instance of '.CurlShared::class);
		}
		
		return \curl_setopt($this->_handle, \CURLOPT_SHARE, $handle->getHandle());
	}
	
}
