<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlPostRedirectionInterface class file.
 * 
 * This class represents the available post redirection methods allowed by curl.
 * 
 * @author Anastaszor
 */
enum CurlPostRedirection : int implements CurlPostRedirectionInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlPostRedirectionInterface::getCurlValue()
	 */
	public function getCurlValue() : int
	{
		return $this->value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlPostRedirectionInterface::with()
	 */
	public function with(CurlPostRedirectionInterface $other) : CurlPostRedirectionInterface
	{
		return new CurlPostRedirectionNode($this, $other);
	}

	case ON_301 = 1;
	case ON_302 = 2;
	case ON_303 = 4;
	
}
