<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlSslAuthTypeNode class file.
 * 
 * This class is to make the possibility to associate multiple ssl auth type
 * compliant objects.
 * 
 * @author Anastaszor
 */
class CurlSslAuthTypeNode implements CurlSslAuthTypeInterface
{
	
	/**
	 * The left size of the tree node.
	 * 
	 * @var CurlSslAuthTypeInterface
	 */
	protected CurlSslAuthTypeInterface $_first;
	
	/**
	 * The right size of the tree node.
	 * 
	 * @var CurlSslAuthTypeInterface
	 */
	protected CurlSslAuthTypeInterface $_second;
	
	/**
	 * Builds a new CurlSslAuthTypeNode with the given other two redirection
	 * objects.
	 * 
	 * @param CurlSslAuthTypeInterface $first
	 * @param CurlSslAuthTypeInterface $second
	 */
	public function __construct(CurlSslAuthTypeInterface $first, CurlSslAuthTypeInterface $second)
	{
		$this->_first = $first;
		$this->_second = $second;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlSslAuthTypeInterface::getCurlValue()
	 */
	public function getCurlValue() : int
	{
		return $this->_first->getCurlValue() | $this->_second->getCurlValue();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlSslAuthTypeInterface::with()
	 */
	public function with(CurlSslAuthTypeInterface $other) : CurlSslAuthTypeInterface
	{
		return new self($this, $other);
	}
	
}
