<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlSslVersion class file.
 * 
 * This class represents the curl ssl versions that are allowed in curl.
 * 
 * @author Anastaszor
 */
enum CurlSslVersion : int implements CurlSslVersionInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlSslVersionInterface::getCurlValue()
	 */
	public function getCurlValue() : int
	{
		return $this->value;
	}

	case DEFAULT = \CURL_SSLVERSION_DEFAULT;
	case SSL_V2 = \CURL_SSLVERSION_SSLv2;
	case SSL_V3 = \CURL_SSLVERSION_SSLv3;
	case TLS_V1 = \CURL_SSLVERSION_TLSv1;
	case TLS_V1_0 = \CURL_SSLVERSION_TLSv1_0;
	case TLS_V1_1 = \CURL_SSLVERSION_TLSv1_1;
	case TLS_V1_2 = \CURL_SSLVERSION_TLSv1_2;
	
}
