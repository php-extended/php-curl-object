<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlCompositeProgressFunction class file.
 * 
 * This class merges two progress functions into one.
 * 
 * @author Anastaszor
 */
class CurlCompositeProgressFunction implements CurlProgressFunctionInterface
{
	
	/**
	 * The first progress function.
	 * 
	 * @var CurlProgressFunctionInterface
	 */
	protected CurlProgressFunctionInterface $_first;
	
	/**
	 * The second progress function.
	 * 
	 * @var CurlProgressFunctionInterface
	 */
	protected CurlProgressFunctionInterface $_second;
	
	/**
	 * Builds a new CurlCompositeProgressFunction with two others progress functions.
	 * 
	 * @param CurlProgressFunctionInterface $first
	 * @param CurlProgressFunctionInterface $second
	 */
	public function __construct(CurlProgressFunctionInterface $first, CurlProgressFunctionInterface $second)
	{
		$this->_first = $first;
		$this->_second = $second;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlProgressFunctionInterface::progress()
	 */
	public function progress(CurlInterface $curl, int $totalDownloadBytes, int $actualDownloadBytes, int $totalUploadBytes, int $actualUploadBytes) : int
	{
		$wt1 = $this->_first->progress($curl, $totalDownloadBytes, $actualDownloadBytes, $totalUploadBytes, $actualUploadBytes);
		$wt2 = $this->_second->progress($curl, $totalDownloadBytes, $actualDownloadBytes, $totalUploadBytes, $actualUploadBytes);
		
		return \max($wt1, $wt2);
	}
	
}
