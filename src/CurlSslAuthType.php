<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlSslAuthType class file.
 * 
 * This class represents the curl ssl auth types that are allowed in curl.
 * 
 * @author Anastaszor
 */
enum CurlSslAuthType : int implements CurlSslAuthTypeInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlSslAuthTypeInterface::getCurlValue()
	 */
	public function getCurlValue() : int
	{
		return $this->value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlSslAuthTypeInterface::with()
	 */
	public function with(CurlSslAuthTypeInterface $other) : CurlSslAuthTypeInterface
	{
		return new CurlSslAuthTypeNode($this, $other);
	}

	case ANY = \CURLSSH_AUTH_ANY;
	case AGENT = \CURLSSH_AUTH_AGENT;
	case HOST = \CURLSSH_AUTH_HOST;
	case PASSWORD = \CURLSSH_AUTH_PASSWORD;
	case PUBLICKEY = \CURLSSH_AUTH_PUBLICKEY;
	case KEYBOARD = \CURLSSH_AUTH_KEYBOARD;
	case NONE = \CURLSSH_AUTH_NONE;
	
}
