<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlProxyType class file.
 * 
 * This class represents the curl proxy types that are allowed in curl.
 * 
 * @author Anastaszor
 */
enum CurlProxyType : int implements CurlProxyTypeInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlProxyTypeInterface::getCurlValue()
	 */
	public function getCurlValue() : int
	{
		return $this->value;
	}

	case HTTP = \CURLPROXY_HTTP;
	case SOCKS4 = \CURLPROXY_SOCKS4;
	case SOCKS4A = \CURLPROXY_SOCKS4A;
	case SOCKS5 = \CURLPROXY_SOCKS5;
	case SOCKS5_HOSTNAME = \CURLPROXY_SOCKS5_HOSTNAME;
	
}
