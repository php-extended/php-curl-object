<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

if(!\class_exists('CurlShareHandle'))
{
	class CurlShareHandle
	{
		// https://www.php.net/manual/en/class.curlhandle.php
	}
}
