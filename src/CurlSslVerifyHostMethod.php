<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlSslVerifyHostMethod class file.
 * 
 * This class represents the curl ssl verify host methods that are allowed in
 * curl.
 * 
 * @author Anastaszor
 */
enum CurlSslVerifyHostMethod : int implements CurlSslVerifyHostMethodInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlSslVerifyHostMethodInterface::getCurlValue()
	 */
	public function getCurlValue() : int
	{
		return $this->value;
	}

	case NO_CHECK = 0;
	case CN_EXISTS = 1;
	case HOSTNAME_MATCHES = 2;
	
}
