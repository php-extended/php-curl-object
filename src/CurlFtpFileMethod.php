<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlFtpFileMethod class file.
 * 
 * This class represents the curl ftp file methods that are allowed in curl.
 * 
 * @author Anastaszor
 */
enum CurlFtpFileMethod : int implements CurlFtpFileMethodInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlFtpFileMethodInterface::getCurlValue()
	 */
	public function getCurlValue() : int
	{
		return $this->value;
	}
	case MULTI_CWD = \CURLFTPMETHOD_MULTICWD;
	case NO_CWD = \CURLFTPMETHOD_NOCWD;
	case SINGLE_CWD = \CURLFTPMETHOD_SINGLECWD;
	
}
