<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlPostRedirectionNode class file.
 * 
 * This class is to make the possibility to associate multiple post redirection
 * compliant objects.
 * 
 * @author Anastaszor
 */
class CurlPostRedirectionNode implements CurlPostRedirectionInterface
{
	
	/**
	 * The left size of the tree node.
	 * 
	 * @var CurlPostRedirectionInterface
	 */
	protected CurlPostRedirectionInterface $_first;
	
	/**
	 * The right size of the tree node.
	 * 
	 * @var CurlPostRedirectionInterface
	 */
	protected CurlPostRedirectionInterface $_second;
	
	/**
	 * Builds a new CurlPostRedirectionNode with the given other two redirection
	 * objects.
	 * 
	 * @param CurlPostRedirectionInterface $first
	 * @param CurlPostRedirectionInterface $second
	 */
	public function __construct(CurlPostRedirectionInterface $first, CurlPostRedirectionInterface $second)
	{
		$this->_first = $first;
		$this->_second = $second;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlPostRedirectionInterface::getCurlValue()
	 */
	public function getCurlValue() : int
	{
		return $this->_first->getCurlValue() | $this->_second->getCurlValue();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlPostRedirectionInterface::with()
	 */
	public function with(CurlPostRedirectionInterface $other) : CurlPostRedirectionInterface
	{
		return new self($this, $other);
	}
	
}
