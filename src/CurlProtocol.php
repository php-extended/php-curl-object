<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlProtocol class file.
 * 
 * This class represents the protocols that are allowed in curl.
 * 
 * @author Anastaszor
 */
enum CurlProtocol : int implements CurlProtocolInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlProtocolInterface::getCurlValue()
	 */
	public function getCurlValue() : int
	{
		return $this->value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlProtocolInterface::with()
	 */
	public function with(CurlProtocolInterface $other) : CurlProtocolInterface
	{
		return new CurlProtocolNode($this, $other);
	}

	case ALL = \CURLPROTO_ALL;
	case DICT = \CURLPROTO_DICT;
	case FILE = \CURLPROTO_FILE;
	case FTP = \CURLPROTO_FTP;
	case FTPS = \CURLPROTO_FTPS;
	case GOPHER = \CURLPROTO_GOPHER;
	case HTTP = \CURLPROTO_HTTP;
	case HTTPS = \CURLPROTO_HTTPS;
	case IMAP = \CURLPROTO_IMAP;
	case IMAPS = \CURLPROTO_IMAPS;
	case LDAP = \CURLPROTO_LDAP;
	case LDAPS = \CURLPROTO_LDAPS;
	case POP3 = \CURLPROTO_POP3;
	case POP3S = \CURLPROTO_POP3S;
	case RTMP = \CURLPROTO_RTMP;
	case RTMPE = \CURLPROTO_RTMPE;
	case RTMPS = \CURLPROTO_RTMPS;
	case RTMPT = \CURLPROTO_RTMPT;
	case RTMPTE = \CURLPROTO_RTMPTE;
	case RTMPTS = \CURLPROTO_RTMPTS;
	case RTSP = \CURLPROTO_RTSP;
	case SCP = \CURLPROTO_SCP;
	case SFTP = \CURLPROTO_SFTP;
	case SMB = \CURLPROTO_SMB;
	case SMBS = \CURLPROTO_SMBS;
	case SMTP = \CURLPROTO_SMTP;
	case SMTPS = \CURLPROTO_SMTPS;
	case TELNET = \CURLPROTO_TELNET;
	case TFTP = \CURLPROTO_TFTP;
	
}
