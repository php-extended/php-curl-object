<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlFile class file.
 * 
 * This class is a regular CURLFile except it implements the CurlFileInterface.
 * 
 * @author Anastaszor
 */
class CurlFile extends \CURLFile implements CurlFileInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see CURLFile::getFilename()
	 * @see CurlFileInterface::getFilename()
	 */
	public function getFilename() : string
	{
		return parent::getFilename();
	}
	
	/**
	 * {@inheritDoc}
	 * @see CURLFile::getMimeType()
	 * @see CurlFileInterface::getMimeType()
	 */
	public function getMimeType() : string
	{
		return parent::getMimeType();
	}
	
	/**
	 * {@inheritDoc}
	 * @see CURLFile::getPostFilename()
	 * @see CurlFileInterface::getPostFilename()
	 */
	public function getPostFilename() : string
	{
		return parent::getPostFilename();
	}
	
}
