<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

use CurlShareHandle;

/**
 * CurlShared class file.
 * 
 * This class is a simple implementation of CurlSharedInterface that uses the
 * underlying curl library.
 * 
 * @author Anastaszor
 */
class CurlShared implements CurlSharedInterface
{
	
	/**
	 * The shared handle for curl.
	 * 
	 * @var CurlShareHandle
	 */
	protected $_handle;
	
	/**
	 * Builds a new CurlShared object.
	 */
	public function __construct()
	{
		$this->_handle = \curl_share_init();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the inner handle to apply it to curl handle.
	 * 
	 * @return CurlShareHandle
	 */
	public function getHandle() : CurlShareHandle
	{
		return $this->_handle;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlSharedInterface::enableSharedCookies()
	 */
	public function enableSharedCookies() : CurlSharedInterface
	{
		/** @psalm-suppress UnusedFunctionCall */
		\curl_share_setopt($this->_handle, \CURLSHOPT_SHARE, (string) \CURL_LOCK_DATA_COOKIE);
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlSharedInterface::disableSharedCookies()
	 */
	public function disableSharedCookies() : CurlSharedInterface
	{
		/** @psalm-suppress UnusedFunctionCall */
		\curl_share_setopt($this->_handle, \CURLSHOPT_UNSHARE, (string) \CURL_LOCK_DATA_COOKIE);
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlSharedInterface::enableSharedDns()
	 */
	public function enableSharedDns() : CurlSharedInterface
	{
		/** @psalm-suppress UnusedFunctionCall */
		\curl_share_setopt($this->_handle, \CURLSHOPT_SHARE, (string) \CURL_LOCK_DATA_DNS);
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlSharedInterface::disableSharedDns()
	 */
	public function disableSharedDns() : CurlSharedInterface
	{
		/** @psalm-suppress UnusedFunctionCall */
		\curl_share_setopt($this->_handle, \CURLSHOPT_UNSHARE, (string) \CURL_LOCK_DATA_DNS);
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlSharedInterface::enableSharedSsl()
	 */
	public function enableSharedSsl() : CurlSharedInterface
	{
		/** @psalm-suppress UnusedFunctionCall */
		\curl_share_setopt($this->_handle, \CURLSHOPT_SHARE, (string) \CURL_LOCK_DATA_SSL_SESSION);
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlSharedInterface::disableSharedSsl()
	 */
	public function disableSharedSsl() : CurlSharedInterface
	{
		/** @psalm-suppress UnusedFunctionCall */
		\curl_share_setopt($this->_handle, \CURLSHOPT_UNSHARE, (string) \CURL_LOCK_DATA_SSL_SESSION);
		
		return $this;
	}
	
	/**
	 * Closes the handle.
	 */
	public function __destruct()
	{
		\curl_share_close($this->_handle);
	}
	
}
