<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

use Exception;

/**
 * CurlException class file.
 * 
 * This exception is to differentiate against other exceptions.
 * 
 * @author Anastaszor
 */
class CurlException extends Exception implements CurlThrowable
{
	
	// nothing to add
	
}
