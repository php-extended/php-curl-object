<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlCompositeReadFunction class file.
 * 
 * This class merges two read functions into one.
 * 
 * @author Anastaszor
 */
class CurlCompositeReadFunction implements CurlReadFunctionInterface
{
	
	/**
	 * The first read function.
	 * 
	 * @var CurlReadFunctionInterface
	 */
	protected CurlReadFunctionInterface $_first;
	
	/**
	 * The second read function.
	 * 
	 * @var CurlReadFunctionInterface
	 */
	protected CurlReadFunctionInterface $_second;
	
	/**
	 * Builds a new CurlCompositeReadFunction with two others write functions.
	 * 
	 * @param CurlReadFunctionInterface $first
	 * @param CurlReadFunctionInterface $second
	 */
	public function __construct(CurlReadFunctionInterface $first, CurlReadFunctionInterface $second)
	{
		$this->_first = $first;
		$this->_second = $second;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlReadFunctionInterface::read()
	 */
	public function read(CurlInterface $curl, CurlFileInterface $file, int $maxlength) : string
	{
		$data = $this->_first->read($curl, $file, $maxlength);
		if(\mb_strlen($data) < $maxlength)
		{
			$data .= $this->_second->read($curl, $file, \max(0, $maxlength - (int) \mb_strlen($data)));
		}
		
		return $data;
	}
	
}
