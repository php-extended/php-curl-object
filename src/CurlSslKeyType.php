<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlSslKeyType class file.
 * 
 * This class represents an enum about the ssl key types.
 * 
 * @author Anastaszor
 */
enum CurlSslKeyType implements CurlSslKeyTypeInterface
{

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlSslKeyTypeInterface::getCurlValue()
	 */
	public function getCurlValue() : string
	{
		return $this->name;
	}

	case PEM;
	case DER;
	case ENG;
	
}
