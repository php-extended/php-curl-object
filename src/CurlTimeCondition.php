<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlTimeCondition class file.
 * 
 * This class represents the curl time conditions that are allowed in curl.
 * 
 * @author Anastaszor
 */
enum CurlTimeCondition : int implements CurlTimeConditionInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlTimeConditionInterface::getCurlValue()
	 */
	public function getCurlValue() : int
	{
		return $this->value;
	}
	
	case IF_UNMOD_SINCE = \CURL_TIMECOND_IFUNMODSINCE;
	case IF_MOD_SINCE = \CURL_TIMECOND_IFMODSINCE;
	
}
