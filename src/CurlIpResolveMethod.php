<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlIpResolveMethod class file.
 * 
 * This class represents the curl ip resolve methods that are allowed in curl.
 * 
 * @author Anastaszor
 */
enum CurlIpResolveMethod : int implements CurlIpResolveMethodInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlIpResolveMethodInterface::getCurlValue()
	 */
	public function getCurlValue() : int
	{
		return $this->value;
	}

	case WHATEVER = \CURL_IPRESOLVE_WHATEVER;
	case IPV4 = \CURL_IPRESOLVE_V4;
	case IPV6 = \CURL_IPRESOLVE_V6;
	
}
