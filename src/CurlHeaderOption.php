<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlHeaderOption class file.
 * 
 * This class represents the curl header option that are allowed in curl.
 * 
 * @author Anastaszor
 */
enum CurlHeaderOption : int implements CurlHeaderOptionInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlHeaderOptionInterface::getCurlValue()
	 */
	public function getCurlValue() : int
	{
		return $this->value;
	}

	case SEPARATED = \CURLHEADER_SEPARATE;
	case UNIFIED = \CURLHEADER_UNIFIED;
	
}
