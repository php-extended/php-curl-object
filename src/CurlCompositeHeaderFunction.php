<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlCompositeHeaderFunction.
 * 
 * This class merges two header functions into one.
 * 
 * @author Anastaszor
 */
class CurlCompositeHeaderFunction implements CurlHeaderFunctionInterface
{
	
	/**
	 * The first header function.
	 * 
	 * @var CurlHeaderFunctionInterface
	 */
	protected CurlHeaderFunctionInterface $_first;
	
	/**
	 * The second header function.
	 * 
	 * @var CurlHeaderFunctionInterface
	 */
	protected CurlHeaderFunctionInterface $_second;
	
	/**
	 * Builds a new CurlCompositeHeaderFunction with two others header functions.
	 * 
	 * @param CurlHeaderFunctionInterface $first
	 * @param CurlHeaderFunctionInterface $second
	 */
	public function __construct(CurlHeaderFunctionInterface $first, CurlHeaderFunctionInterface $second)
	{
		$this->_first = $first;
		$this->_second = $second;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlHeaderFunctionInterface::write()
	 */
	public function write(CurlInterface $curl, string $headerStr) : int
	{
		$wt1 = $this->_first->write($curl, $headerStr);
		$wt2 = $this->_second->write($curl, $headerStr);
		
		return \max($wt1, $wt2);
	}
	
}
