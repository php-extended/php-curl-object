<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlCompositePasswordFunction class file.
 * 
 * This class merges two password functions into one.
 * 
 * @author Anastaszor
 */
class CurlCompositePasswordFunction implements CurlPasswordFunctionInterface
{
	
	/**
	 * The first password function.
	 * 
	 * @var CurlPasswordFunctionInterface
	 */
	protected CurlPasswordFunctionInterface $_first;
	
	/**
	 * The second password function.
	 * 
	 * @var CurlPasswordFunctionInterface
	 */
	protected CurlPasswordFunctionInterface $_second;
	
	/**
	 * Builds a new CurlCompositePasswordFunction with two others password functions.
	 * 
	 * @param CurlPasswordFunctionInterface $first
	 * @param CurlPasswordFunctionInterface $second
	 */
	public function __construct(CurlPasswordFunctionInterface $first, CurlPasswordFunctionInterface $second)
	{
		$this->_first = $first;
		$this->_second = $second;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Curl\CurlPasswordFunctionInterface::prompt()
	 */
	public function prompt(CurlInterface $curl, string $prompt, int $maxlength) : string
	{
		$pwd = $this->_first->prompt($curl, $prompt, $maxlength);
		if(empty($pwd))
		{
			$pwd = $this->_second->prompt($curl, $prompt, $maxlength);
		}
		
		return $pwd;
	}
	
}
