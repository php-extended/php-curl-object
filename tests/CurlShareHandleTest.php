<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PHPUnit\Framework\TestCase;

/**
 * CurlShareHandleTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Curl\CurlShareHandle
 * @internal
 * @small
 */
class CurlShareHandleTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CurlShareHandle
	 */
	protected $_object;
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CurlShareHandle();
	}
	
}
