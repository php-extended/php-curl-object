<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Curl\CurlIpResolveMethod;
use PHPUnit\Framework\TestCase;

/**
 * CurlIpResolveMethodTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Curl\CurlIpResolveMethod
 *
 * @internal
 *
 * @small
 */
class CurlIpResolveMethodTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CurlIpResolveMethod
	 */
	protected CurlIpResolveMethod $_object;
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void {}
	
}
