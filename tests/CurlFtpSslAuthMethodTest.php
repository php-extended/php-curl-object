<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Curl\CurlFtpSslAuthMethod;
use PHPUnit\Framework\TestCase;

/**
 * CurlFtpSslAuthMethodTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Curl\CurlFtpSslAuthMethod
 *
 * @internal
 *
 * @small
 */
class CurlFtpSslAuthMethodTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CurlFtpSslAuthMethod
	 */
	protected CurlFtpSslAuthMethod $_object;
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void {}
	
}
