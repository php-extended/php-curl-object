<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Curl\CurlSslAuthTypeNode;
use PHPUnit\Framework\TestCase;

/**
 * CurlSslAuthTypeNodeTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Curl\CurlSslAuthTypeNode
 *
 * @internal
 *
 * @small
 */
class CurlSslAuthTypeNodeTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CurlSslAuthTypeNode
	 */
	protected CurlSslAuthTypeNode $_object;
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void {}
	
}
