<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Curl\CurlPostRedirectionNode;
use PHPUnit\Framework\TestCase;

/**
 * CurlPostRedirectionNodeTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Curl\CurlPostRedirectionNode
 *
 * @internal
 *
 * @small
 */
class CurlPostRedirectionNodeTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CurlPostRedirectionNode
	 */
	protected CurlPostRedirectionNode $_object;
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void {}
	
}
