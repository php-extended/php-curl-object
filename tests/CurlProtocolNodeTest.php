<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Curl\CurlProtocolNode;
use PHPUnit\Framework\TestCase;

/**
 * CurlProtocolNodeTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Curl\CurlProtocolNode
 *
 * @internal
 *
 * @small
 */
class CurlProtocolNodeTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CurlProtocolNode
	 */
	protected CurlProtocolNode $_object;
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void {}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::tearDown()
	 */
	protected function tearDown() : void
	{
		$this->_object = null;
	}
	
}
