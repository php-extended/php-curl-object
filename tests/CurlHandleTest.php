<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PHPUnit\Framework\TestCase;

/**
 * CurlHandleTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Curl\CurlHandle
 * @internal
 * @small
 */
class CurlHandleTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CurlHandle
	 */
	protected $_object;
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CurlHandle();
	}
	
}
