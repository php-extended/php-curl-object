<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Curl\CurlCompositeReadFunction;
use PHPUnit\Framework\TestCase;

/**
 * CurlCompositeReadFunctionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Curl\CurlCompositeReadFunction
 *
 * @internal
 *
 * @small
 */
class CurlCompositeReadFunctionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CurlCompositeReadFunction
	 */
	protected CurlCompositeReadFunction $_object;
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void {}
	
}
