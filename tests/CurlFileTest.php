<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Curl\CurlFile;
use PHPUnit\Framework\TestCase;

/**
 * CurlFileTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Curl\CurlFile
 *
 * @internal
 *
 * @small
 */
class CurlFileTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CurlFile
	 */
	protected CurlFile $_object;
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void {}
	
}
