<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Curl\CurlCompositeProgressFunction;
use PHPUnit\Framework\TestCase;

/**
 * CurlCompositeProgressFunctionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Curl\CurlCompositeProgressFunction
 *
 * @internal
 *
 * @small
 */
class CurlCompositeProgressFunctionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CurlCompositeProgressFunction
	 */
	protected CurlCompositeProgressFunction $_object;
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void {}
	
}
