<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Curl\CurlCompositePasswordFunction;
use PHPUnit\Framework\TestCase;

/**
 * CurlCompositePasswordFunctionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Curl\CurlCompositePasswordFunction
 *
 * @internal
 *
 * @small
 */
class CurlCompositePasswordFunctionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CurlCompositePasswordFunction
	 */
	protected CurlCompositePasswordFunction $_object;
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void {}
	
}
