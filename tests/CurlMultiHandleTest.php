<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PHPUnit\Framework\TestCase;

/**
 * CurlMultiHandleTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Curl\CurlMultiHandle
 * @internal
 * @small
 */
class CurlMultiHandleTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CurlMultiHandle
	 */
	protected $_object;
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CurlMultiHandle();
	}
	
}
