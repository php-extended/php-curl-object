<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Curl\CurlHttpAuthMethod;
use PHPUnit\Framework\TestCase;

/**
 * CurlHttpAuthMethodTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Curl\CurlHttpAuthMethod
 *
 * @internal
 *
 * @small
 */
class CurlHttpAuthMethodTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CurlHttpAuthMethod
	 */
	protected CurlHttpAuthMethod $_object;
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void {}
	
}
