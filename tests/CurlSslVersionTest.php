<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Curl\CurlSslVersion;
use PHPUnit\Framework\TestCase;

/**
 * CurlSslVersionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Curl\CurlSslVersion
 *
 * @internal
 *
 * @small
 */
class CurlSslVersionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CurlSslVersion
	 */
	protected CurlSslVersion $_object;
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void {}
	
}
