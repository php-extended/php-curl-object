# php-extended/php-curl-object
A library that implements the php-curl-interface library

![coverage](https://gitlab.com/php-extended/php-curl-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-curl-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-curl-object ^8`


## Basic Usage

This library may be used the following way :

```

use PhpExtended\Curl\Curl;
use PhpExtended\HttpMessage\Uri;

$curl = new Curl();
$curl->setUri(Uri::parseFromString('<your uri there>'));
$curl->execute();
$data = $curl->getData();	// returns a Psr\Http\Message\StreamInterface
$str = $data->__toString();

```


## License

MIT (See [license file](LICENSE)).
